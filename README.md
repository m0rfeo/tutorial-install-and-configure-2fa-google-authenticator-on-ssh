---
author: m0rfeo
title: Two-factor Authentication on ssh with Google Authenticator
---

Intro
=========================

**Please read the entire document in detal, is important to know and
undestand how this 2FA method works or could make it difficult to access
the server.**

2FA is really needed in these times, it add another layer of security to
critical services like SSH. We will describe in the following document
how to implement and configure 2FA using google-authenticator PAM Module
for ssh connections.

Google Authenticator is based on TOTP (Time-Based-One-Time Password), an
open standard that rotates secure tokens in discrete time blocks (30s).

To access to this TOTP we have to generate QR code on selected computer
and scan it from Google Authenticator APP.

**THIS TOTP IS TOTALLY UNIQUE FOR ONE USER OF THE SYSTEM**

Componets
=============================

-   Package installed: libpam-google-authenticator

-   Mobile App: Google Authenticator (iOS/Android)

Install Google Authenticator library.
=========================================================

``` {.example}
apt install libpam-google-authenticator
```

Configure SSHD to require Google Authenticator
==================================================================

We have to edit PAM modules file for SSHD.

``` {.example}
sudo nano /etc/pam.d/sshd
```

Inside the document add this parameter to require the module previously
installed. This file is readed sequentially, so add at the firts line to
requires 2FA firts and password after (more safe). **NULLOK MAKE TO THE
USERS THAT DON\'T GENERATE QR CODE CAN PUT PASSWORD WITHOUT 2FA. EASIER
TO MANTAIN BUT LESS SECURE.** **WITHOUT THIS PARAMETER 2FA WILL BE
REQUIRED TO ALL USERS AT THE MOMENT IT IS ACTIVATED**

``` {.example}
auth required pam_google_authenticator.so nullok
```

Now we have to edit SSHD conf file to activate Challenge Response
Authentication.

``` {.example}
sudo nano /etc/ssh/sshd_config
```

We have to set this parameter in the file to yes to expect and
authentication code instead of accepting user and password. If you
don\'t find this parameter on the document don\'t worry just add it.

``` {.example}
ChallengeResponseAuthentication yes
```

Finally remember to restart SSHD service and check the status:

``` {.example}
sudo systemctl restart sshd.service

sudo systemctl status sshd.service (NOT ERRORS EXPECTED)
```

Generate QR on computer and configure Google Authenticator
==============================================================================

Run the following instruction and say yes to the next question to make
the TOTP change every 30s.

``` {.example}
google-authenticator
Do you want authentication tokens to be time-based (y/n) y
```

At this moment a QR code, and URL that shows this QR and a **SECRET
KEY** that is equivalent to the QR code will be displayed on the screen
and we have to scan the QR from google authenticator app on our mobile
device. Follow these steps and please save the secret key generated in a
secure way as a paper. **NEVER OPEN THE URL, THIS EXPOSE YOUR QR TO YOUR
BROWSER AND THIS LET REGISTRY ON DNS CACHE AND AN SNIFFER COULD BE
LISTENING ON THE NETWOR**

``` {.example}
- ON THE SMARTPHONE:
1. Download Google Authenticator APP on Play Store (Android) or App Store (iOS).
2. Open the APP and press the + button at the bottom of the screen, select scan QR code and scan the QR generated on the computer.
3. This will create a account of the user where the command was executed on the mobile phone.
4. Click on the session created to see the PIN.

- ON THE COMPUTER:
1. The google-authenticator is asking for the PIN on computer to confirm the account.
2. Type the PIN displayed on the mobile APP (be careful, it changes every 30s).

- ADDITIONAL:
1. Make sure that you save the secret key in a secure way as a paper, AND BE CAREFUL TO DON'T MAKE MISTAKES!!!
YOU WILL NEED THIS SECRET KEY IN THE FUTURE IF YOU WANT TO ACTIVATE THE 2FA FOR YOUR USER IN ANOTHER MOBILE DEVICE
```

One time we have our account on the mobile APP we have to say yes to the
next question too

``` {.example}
Do you want me to update your "/home/m0rfeo/.google_authenticator" file? (y/n) y
```

Say no to the following question, this make that only one user can use
one token (ONE LOGIN EVERY 30 SECONDS). If you want to improve more the
security say yes.

``` {.example}
Do you want to disallow multiple uses of the same authentication
token? This restricts you to one login about every 30s, but it increases
your chances to notice or even prevent man-in-the-middle attacks (y/n) n
```

Say no to the next question, say yes on this parameter could give more
chances to an attacker to obtain a valid token

``` {.example}
 This will permit for a time skew of up to 4 minutes
between client and server.
Do you want to do so? (y/n) n 
```

Finally say yes to the last question to enable rate-limiting to try
tokens

``` {.example}
By default, this limits attackers to no more than 3 login attempts every 30s.
Do you want to enable rate-limiting? (y/n) y
```

Adding more mobile devices for a system account
===================================================================

At this point we will need the secret key generated previously for the
account. Just go to Google Authenticator APP and click to add account,
click to + button and select enter configuration key, select a name for
your account and type the secret key previously saved

Adding more than a system account on one mobile device
==========================================================================

We just have to know the secret key or scan QR code for that user and
add configuration key/QR on Google Authenticator APP .

Final result
================================

Now when you try to login with ssh, if you type the correct password
additionally you have to type the correct PIN. **This step will not be
required if you have your public key on the server, because the order by
default on ssh to check authentication method is public key and later
password.**

``` {.example}
╰─➤  ssh localhost                                                                  
(m0rfeo@localhost) Verification code: 
(m0rfeo@localhost) Password: 
```

Possible vectors of attack
==============================================

Here we will describe some possible actions that can break our 2FA.

-   If the attacker really want to enter to the server firts need to
    know a valid user and later have to identify the person behind that
    user. The attacker will try to do some social engineering tactics to
    access to the phone of this person\'s and can execute commands to
    see the TOTP, to to that the phone have to been rooted and he will
    still need the password for this person and remember that the code
    change every 30s, be hurry hacker...

-   When we generate a QR to get TOTP on the mobile app we generate too
    a URL where we can see the QR every time we want. DON\'T PUT THIS
    URL ON NAVIGATOR, THIS EXPOSE THE URL ON THE NAVIGATOR FOR A
    POSSIBLY ATTACKER OR SNIFFER ON THE NETWORK!!!

-   In the same way if we expose our secret key to any person this
    person will be have the correct token for your 2FA too!!!
